<?php 

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$domba = new Animal("Shaun");
echo "Name = ".$domba->name . "<br>";
echo "legs = ".$domba->leg . "<br>";
echo "Cold Blooded = ".$domba->cold_blooded . "<br><br>";

$kodok = new Frog("buduk");
echo "Name = ".$kodok->name . "<br>";
echo "legs = ".$kodok->leg . "<br>";
echo "Cold Blooded = ".$kodok->cold_blooded . "<br>";
echo "Jump = ";$kodok->jump();

echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name = ".$sungokong->name . "<br>";
echo "legs = ".$sungokong->leg . "<br>";
echo "Cold Blooded = ".$sungokong->cold_blooded . "<br>";
echo "Yell = ";$sungokong->yell();



?>